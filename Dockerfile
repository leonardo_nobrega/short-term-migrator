FROM ship.cenx.com:5000/clojure
COPY target/uberjar/short-term-migrator-0.1.0-standalone.jar \
     /app/target/uberjar/
COPY run /app/run
WORKDIR /app
