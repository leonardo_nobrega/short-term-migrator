(ns short-term-migrator.core
  (:require [clojure.core.async :as async]
            [clojure.pprint :refer [pprint]]
            [clojure.string :as string]
            [clojure.tools.cli :as cli]
            [qbits.alia :as alia]
            [qbits.hayt :as hayt]
            [taoensso.timbre :as log])
  (:import (java.net InetAddress))
  (:gen-class))

;;; progress logging

(def time-to-print-ms 2000)

(defn time-ms->string
  [time-ms]
  (let [hour (quot time-ms (* 60 60 1000))
        minutes-in-ms (- time-ms (* hour 60 60 1000))
        minute (quot minutes-in-ms (* 60 1000))
        seconds-in-ms (- minutes-in-ms (* minute 60 1000))
        second (quot seconds-in-ms 1000)]
    (format "%02d:%02d:%02d" hour minute second)))

(defn elapsed-time
  [current-state]
  (->> current-state
       :start-time
       (- (System/currentTimeMillis))
       time-ms->string))

(defn percentage
  [current-state]
  (->> (count (:all-ids current-state))
       (/ (inc (.indexOf (:all-ids current-state) (:current current-state))))
       (* 100)
       double
       (format "%.2f")))

(defn format-long-number
  [num]
  (let [format-parts (fn [parts]
                       (list* (-> parts first str)
                              (->> parts rest (map #(format "%03d" %)))))]
    (loop [current num
           parts (list)]
      (let [q (quot current 1000)
            new-parts (cons (rem current 1000) parts)]
        (if (> q 0)
          (recur q new-parts)
          (->> new-parts
               format-parts
               (interpose ",")
               (apply str)))))))

(defn print-state
  [current-state]
  (-> current-state
      (assoc :elapsed (elapsed-time current-state))
      (assoc :percentage-of-ids (percentage current-state))
      (update :num-rows format-long-number)
      (dissoc :all-ids)
      (dissoc :start-time)
      pprint
      with-out-str
      (#(log/info "current state\n" %))))

(defn start-status-thread
  "Periodically prints the state of a process."
  [progress-channel]
  (async/go-loop [state {}
                  timer (async/timeout time-to-print-ms)]
    (let [[msg ch] (async/alts! [timer progress-channel])]
      (cond
        ;; timed out:
        ;; print state, start a new timer
        (= ch timer)
        (do (print-state state)
            (recur state (async/timeout time-to-print-ms)))

        ;; work is complete:
        ;; print state and return
        (= msg :end) :done

        ;; did not time out and did not finish working
        ;; (received a new state):
        ;; update the state, keep waiting on the same timer
        :else
        (recur msg timer)))))

;;; table schema

(def source-table-name :vnf_data)
(def source-columns
  {:id :text
   :idfamily :text
   :week :bigint
   :metric :text
   :timestamp :bigint
   :vtext :text
   :vlong :bigint
   :vdouble :double
   :primary-key [[:id :idfamily :week] :metric :timestamp]
   :clustering-order [[:metric :asc]
                      [:timestamp :desc]]})

(def target-table-name :short_term)
(def target-columns
  ;; target columns are different from source columns in the clustering key
  ;; (source table has metric, timestamp, target has timestamp, metric
  (-> source-columns
      (assoc :primary-key [[:id :idfamily :week] :timestamp :metric])
      (assoc :clustering-order [[:timestamp :desc]
                                [:metric :asc]])))

;;; migration

(defn table-with-keyspace
  [keyspace-str table-name-kw]
  (keyword (str keyspace-str "." (name table-name-kw))))

(defn get-rows-to-migrate
  "Extracts an id and week from the current-state and
  uses them to query the source table."
  [cassandra-session keyspace-str current-state]
  (->> current-state
       :current
       hayt/where
       (hayt/select (table-with-keyspace keyspace-str source-table-name))
       (alia/execute cassandra-session)))

(defn make-insert
  "row should be a map having the keys: id, idfamily, metric, timestamp, vdouble"
  [table-name row]
  (hayt/insert table-name (hayt/values row)))

(defn batch-of-row-inserts
  [batch-size table-name rows]
  (let [make-batch (fn [inserts]
                     (hayt/batch (apply hayt/queries inserts)
                                 (hayt/logged false)))]
    (->> rows
         (map (partial make-insert table-name))
         (partition batch-size batch-size nil)
         (map make-batch))))

(defn migrate-one-id
  "Migrates all rows with the same id-week."
  [progress-channel cassandra-session keyspace-str current-state]
  (let [batch-size 100
        rows (get-rows-to-migrate cassandra-session keyspace-str current-state)]

    ;; update the status thread with the current state
    (async/>!! progress-channel current-state)

    ;; write to target table
    (->> rows
         ;; make batches
         (batch-of-row-inserts batch-size target-table-name)
         ;; write them
         (list* (hayt/use-keyspace keyspace-str))
         (map #(alia/execute cassandra-session %))
         dorun)

    ;; update num-rows in current-state
    (update current-state :num-rows #(+ (count rows) %))))

(defn get-all-ids
  "Gets all id-week tuples in the table.
  This gives us a way to iterate on the table."
  [cassandra-session keyspace-str]
  (let [table (table-with-keyspace keyspace-str source-table-name)]
    (as-> table $
      (hayt/select $ (->> source-columns
                          :primary-key
                          first
                          (apply hayt/distinct*)
                          hayt/columns))
      (alia/execute cassandra-session $)
      ;; sort ids to do them in order
      ;; if a failure happens, we will know from where to restart
      (sort-by (juxt :id :week) $))))

(defn get-all-ids-if-needed
  "Adds the list of all ids to the initial state."
  [cassandra-session keyspace-str initial-state]
  (cond-> initial-state
    (not (contains? initial-state :start-time))
    (assoc :start-time (System/currentTimeMillis))

    (not (contains? initial-state :all-ids))
    (assoc :all-ids (get-all-ids cassandra-session keyspace-str))

    ;; if the initial state has a current value,
    ;; drop id-week tuples from all-ids
    ;; (in case of problems, this lets us start closer to the point where we failed)
    (:current initial-state)
    (update :all-ids #(-> % (.indexOf (:current initial-state)) (drop %)))

    true (assoc :num-rows 0)))

(defn create-table
  [session keyspace table-name columns]
  (let [ttl-ten-days (* 10 24 60 60)
        keyspace-props {:replication {:replication_factor 1
                                      :class "SimpleStrategy"}}
        table-props (-> {:default_time_to_live ttl-ten-days}
                        (assoc :clustering-order
                               (:clustering-order columns)))
        commands (list (hayt/create-keyspace keyspace
                                             (hayt/if-exists false)
                                             (hayt/with keyspace-props))
                       (hayt/use-keyspace keyspace)
                       (hayt/create-table table-name
                                          (hayt/if-not-exists)
                                          (hayt/column-definitions (dissoc columns :clustering-order))
                                          (hayt/with table-props)))]
    (log/info "create-table:" (->> commands (map hayt/->raw) seq))
    (->> commands
         (map (partial alia/execute session))
         dorun)))

(defn migrate-with-session
  ([cassandra-session keyspace-str]
   (migrate-with-session cassandra-session keyspace-str {}))

  ([cassandra-session keyspace-str initial-state]
   (create-table cassandra-session
                 keyspace-str
                 target-table-name
                 target-columns)
   (let [progress-channel (async/chan)
         current-state (get-all-ids-if-needed cassandra-session
                                              keyspace-str
                                              initial-state)]
     (start-status-thread progress-channel)
     (try
       (->> current-state
            :all-ids
            ;; migrates id by id and keeps track of the number of rows written
            (reduce (fn [state id-week]
                      (->> id-week
                           (assoc state :current)
                           (migrate-one-id progress-channel
                                           cassandra-session
                                           keyspace-str)))
                    current-state)
            print-state)
       (catch Exception e
         (log/error e
                    "Exception caught. Current state: %s"
                    (-> current-state pprint with-out-str)))
       (finally (async/>!! progress-channel :end))))
   ;; give the status thread a little time to print the last state
   (Thread/sleep 100)))

(defn make-session
  ([]
   (make-session "localhost"))

  ([cassandra-address]
   (->> cassandra-address
        (hash-map :contact-points)
        alia/cluster
        alia/connect)))

(defn migrate
  "Reads from the source table, writes to the target table."
  [cassandra-address keyspace-str initial-state]
  (let [session (make-session cassandra-address)]
    (migrate-with-session session keyspace-str initial-state)))

(defn migrate-with-cli-args
  [args]
  (migrate (:cassandra-address args)
           (:keyspace-str args)
           (:initial-state args)))

;;; main

;; based on
;; https://github.com/clojure/tools.cli#example-usage

(def cli-options
  [["-s"
    "--start-from ID-IDFAMILY-KEY"
    "starting point: clojure map containing :id, :idfamily and :week"
    :parse-fn read-string
    :validate [#(= (-> % keys set) #{:id :idfamily :week})
               "must be a map containing :id, :idfamily and :week"]]])

(defn usage [options-summary]
  (->> ["Moves data from vnf_data to short_term."
        ""
        "Usage: short-term-migrator [options] CASSANDRA_ADDRESS KEYSPACE"
        ""
        "Options:"
        options-summary
        ""
        "More information at:"
        "https://bitbucket.org/leonardo_nobrega/short-term-migrator"
        ""]
       (string/join \newline)))

(defn error-msg
  [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (string/join \newline errors)))

(defn validate-args
  "Validate command line arguments. Either return a map indicating the program
  should exit (with a error message, and optional ok status), or a map
  indicating the action the program should take and the options provided."
  [args]
  (let [{:keys [arguments options errors summary]} (cli/parse-opts args cli-options)
        options-set (-> options keys set)]
    (cond
      errors ; errors => exit with description of errors
      {:exit-message (error-msg errors)}

      (:help options) ; help => exit OK with usage summary
      {:exit-message (usage summary) :ok? true}

      ;; custom validation on arguments
      (and (= (count arguments) 2)
           (-> arguments
               first
               (InetAddress/getByName)
               (try (catch Exception e false)))
           (or (empty? options)
               (= (-> options keys first) :start-from)))
      {:cassandra-address (first arguments)
       :keyspace-str (second arguments)
       :initial-state (->> options :start-from (array-map :current))}

      :else ; failed custom validation => exit with usage summary
      {:exit-message (usage summary)})))

(defn exit
  [status msg]
  (println msg)
  (System/exit status))

(defn -main [& args]
  (let [validated-args (validate-args args)]
    (if-let [exit-message (:exit-message validated-args)]
      (exit (if (:ok? validated-args) 0 1) exit-message)
      (do (migrate-with-cli-args validated-args)
          (System/exit 0)))))
