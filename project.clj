(defproject short-term-migrator "0.1.0"
  :description "Moves data from vnf_data to short_term."
  :url "https://bitbucket.org/leonardo_nobrega/short-term-migrator"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/tools.cli "0.3.7"]
                 [cc.qbits/alia-all "3.1.11" :exclusions [cc.qbits/tardis]]
                 [cc.qbits/hayt "3.1.0"]
                 [com.taoensso/timbre "4.1.1"]]
  :main short-term-migrator.core
  :aot :all
  :target-path "target/%s"
  :profiles {:repl {:main short-term-migrator.core
                    :target-path "target"}})
