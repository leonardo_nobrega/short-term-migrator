# short-term-migrator

Done for [CD-48927](https://cenx-cf.atlassian.net/browse/CD-48927).

We are changing the schema of the short-term table (currently called vnf_data) so that it will be possible to query the rows between two points in time without having to specify metrics.

This program creates a new table (called short_term), then moves the contents of vnf_data to it.

The migrator works id-by-id (in the context of VCP, an id is the name of a VNF). It will execute a query on vnf_data to find all id-idfamily-week slices in the table. Then it iterates over them: it reads all samples for one id-idfamily-week, then writes them to the new table.

The program took 16 minutes on a dev machine to move the data corresponding to 1000 VNFs (10 days of data, each VNF providing one sample per minute).

## Installation

Clone the repo: https://bitbucket.org/leonardo_nobrega/short-term-migrator

Build a standalone jar with `lein uberjar`

Move the code to a dev machine

    $ rsync -r * deployer@your-dev-machine.cenx.localnet:/home/deployer/short-term-migrator

Create a docker image

    [deployer@your-dev-machine]$ docker build -t short-term-migrator .

## Usage

Local run from the deployer's machine:

    $ ./short-term-migrator [-s ID-IDFAMILY-KEY] CASSANDRA_ADDRESS KEYSPACE

Run as a docker container on a dev machine:

    [deployer@your-dev-machine]$ docker run -d --name stm short-term-migrator \
    > /app/run [-s ID-IDFAMILY-KEY] CASSANDRA_ADDRESS KEYSPACE

Tail the log while the container runs:

    [deployer@your-dev-machine]$ docker exec -it stm tail -f /app/log.txt

Copy the log after it finishes:

    [deployer@your-dev-machine]$ docker cp stm:/app/log.txt .

Remove the container:

    [deployer@your-dev-machine]$ docker rm stm

## Parameters

* CASSANDRA_ADDRESS: where the vnf_data (source table) is.
* KEYSPACE: name of the keyspace containing the vnf_data table.

The migrator will create the target table (short-term) in the same Cassandra and keyspace.

## Options

-s (starting point): this lets us start from the given id-idfamily-week slice in case of a failure during the migration.

    $ ./short-term-migrator -s '{:id "some-id" :idfamily "vim" :week 2517}'
    > your-cassandra.cenx.localnet your_keyspace

    [deployer@your-dev-machine]$ docker run -d --name stm short-term-migrator \
    > /app/run -s '{:id "some-id" :idfamily "vim" :week 2517}' \
    > your-cassandra.cenx.localnet your_keyspace

## License

Copyright © 2018 CENX
